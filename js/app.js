
inicioSesion();
// Funcion que carga el usuario en el head
function inicioSesion() {
    var usuario = sessionStorage.getItem('usuario');
        document.getElementById("userHead").innerHTML = usuario;
    // var token = sessionStorage.getItem('token');
        var userObj = JSON.parse(sessionStorage.getItem('userObj'));
    if (!userObj.token) {
        window.location = 'index.html';
    }
}

// Funciones para la barra de menú y navegación
// closeSidebar();
// function openSidebar() {
//     document.getElementById("sidebar").style.display = "block";
// }
// function closeSidebar() {
//     document.getElementById("sidebar").style.display = "none";
// }

function cerrarSesion() {
    localStorage.removeItem('favoritos');
    sessionStorage.clear();
    window.location = 'index.html';
}

// Funcion que meustra el detalle de una pelicula
function detalle(id) {
    window.location.href = "detalle.html?id=" + id;
}

// Objeto que uso para agregar peliculas a favoritos
function Pelicula(id, imdbID, nombre, ano, imagen) {
    this.id = id;
    this.imdbID = imdbID;
    this.nombre = nombre;
    this.ano = ano;
    this.imagen = imagen;
}

// Funcion que agrega al localstorage y al array de favoritos
function favorito(id) {
    if (localStorage.getItem('favoritos')) {
        var favoritos = JSON.parse(localStorage.getItem('favoritos'));
    } else {
        // Array donde se guardan los favoritos
        var favoritos = [];
    }

    if (id != "") {
        var url = "http://www.omdbapi.com/?apikey=f12ba140&i=" + id + "&r=json";
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var json = JSON.parse(this.responseText);
//Compruebo que la pelicula
console.log(favoritos);

                var pelicula = new Pelicula(favoritos.length + 1, json.imdbID, json.Title, json.Year, json.Poster);
                favoritos.push(pelicula);
                localStorage.setItem('favoritos', JSON.stringify(favoritos));

                if (json.Response === 'False') {
                    alert('No se ha podido añadir la pelicula, error: ' + json.Error);
                }
            }
        };
        xhttp.open("GET", url, true);
        xhttp.send();
    }
}


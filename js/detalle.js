document.getElementById("errorId").style.display = "none";
var sPaginaURL = window.location.search.substring(1);
var parametros = sPaginaURL.split('id=');

detallePelicula(parametros[1]);
function detallePelicula(id) {
    if (id != "") {
        var url = "http://www.omdbapi.com/?apikey=f12ba140&i=" + id + "&r=json";
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var json = JSON.parse(this.responseText);
                pintarHtml(json);
                if (json.Response === 'False') {
                    document.getElementById("errorId").style.display = "block";
                    document.getElementById("errorCod").innerHTML = json.Error;
                }
            }
        };
        xhttp.open("GET", url, true);
        xhttp.send();
    } else {
        document.getElementById("errorId").style.display = "block";
    }
}

function pintarHtml(json) {
    document.getElementById("imgPel").src = json.Poster;
    document.getElementById("title").innerHTML = json.Title;
    document.getElementById("director").innerHTML = json.Director;
    document.getElementById("country").innerHTML = json.Country;
    document.getElementById("runtime").innerHTML = json.Runtime;
    document.getElementById("rating").innerHTML = json.imdbRating;
}
window.onload = function () {
    document.getElementById("errorTipo").style.display = "none";
}
function pintarHtml(json) {
    for (var i = 0; i < json.Search.length; i++) {
        if (json.Search[i].Poster === 'N/A') {
            img = "../img/disponible.png";
        } else {
            img = json.Search[i].Poster;
        }

        document.getElementById("demo").innerHTML = document.getElementById("demo").innerHTML
            + "<div class='col-md-6 col-lg-3'>"
            + " <a class='portfolio-item d-block mx-auto'>"
            + " <div class='portfolio-item-caption d-flex position-absolute h-100 w-100'>"
            + " <div class='portfolio-item-caption-content my-auto w-100 text-center text-white'>"
            + " <i class='fa fa-search-plus fa-3x' onclick=detalle('" + json.Search[i].imdbID + "')></i>"
            + " <i class='fa fa-star fa-3x' onclick=favorito('" + json.Search[i].imdbID + "')></i>"
            + " </div>"
            + " </div>"
            + " <img class='img-fluid cartelera' src='" + img + "' alt='"+ json.Search[i].Title +"' title='"+ json.Search[i].Title +"'>"
            + " </a>"
            + " </div>"

        // + "<hr/>"
        // + "<div class='w3-cell-row'>"
        // + "<div class='w3-cell' style='width:30%'>"
        // + "<img class='' src='" + img + "' style='width:100%'>"
        // + "</div>"
        // + "<div class='w3-cell w3-container'>"
        // + "<h4>" + json.Search[i].Title + "</h4>"
        // + "<p> <strong>Año:</strong> " + json.Search[i].Year + "</p>"
        // + "<i class='fas fa-info fa-lg' onclick=detalle('" + json.Search[i].imdbID + "')>"
        // + "</i> <i class='fas fa-heart fa-lg ml-3' onclick=favorito('" + json.Search[i].imdbID + "')></i>"
        // + "</div>"
        // + "</div>"

    }
}

function buscarPelicula() {
    document.getElementById("demo").innerHTML = " ";
    var tipo = document.getElementById('tipo').value;
    if (tipo != "") {
        document.getElementById("errorTipo").style.display = "none";
        var url = "http://www.omdbapi.com/?apikey=f12ba140&s=" + tipo + "&r=json";
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var json = JSON.parse(this.responseText);
                pintarHtml(json);
            }
        };
        xhttp.open("GET", url, true);
        xhttp.send();
    } else {
        document.getElementById("errorTipo").style.display = "block";
    }
}






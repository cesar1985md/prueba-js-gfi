listarDetalle();
function listarDetalle() {
    var favoritos = JSON.parse(localStorage.getItem('favoritos'));
    favoritos.forEach(function (element) {
        if (element.imagen === 'N/A') {
            img = "https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Imagen_no_disponible.svg/1024px-Imagen_no_disponible.svg.png";
        } else {
            img = element.imagen;
        }
        document.getElementById("favoritos").innerHTML = document.getElementById("favoritos").innerHTML

            + "<div class='col-md-6 col-lg-3'>"
            + " <a class='portfolio-item d-block mx-auto'>"
            + " <div class='portfolio-item-caption d-flex position-absolute h-100 w-100'>"
            + " <div class='portfolio-item-caption-content my-auto w-100 text-center text-white'>"
            + " <i class='fa fa-search-plus fa-3x' onclick=detalle('" + element.imdbID + "')></i>"
            + " </div>"
            + " </div>"
            + " <img class='img-fluid cartelera' src='" + img + "' alt='" + element.Title + "' title='" + element.Title + "'>"
            + " </a>"
            + " </div>"
        // + "<li class='table-view-cell media' >"
        // + "<a class='navigate' data-transition='slide-in'>"
        // + "<img class='media-object pull-left' src='" + img + "' style='width:64px; height:64px'>"
        // + "<div class='media-body'>"
        // + element.Title
        // + "<p> <strong>Año:</strong> " + element.Year + "</p>"
        // + "<span class='icon icon-star' onclick=favorito('" + element.imdbID + "')></span>"
        // + "<span class='icon icon-info' style='float:right' onclick=detalle('" + element.imdbID + "')></span>"
        // + "</div>"
        // + "</a>"
        // + "</li>"
    });
}